import tensorflow as tf

sess = tf.Session()

x = tf.constant([[0.5, 1]])
b = tf.acos(x)
print(sess.run(b))
