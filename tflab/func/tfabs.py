import tensorflow as tf

"""
abs 定义在复数上的，实部的平+虚部的平方和开根号
"""


sess = tf.Session()
x = tf.constant([[-2.25+4.475j], [-3.25 + 5.75j]])
x_2 = tf.constant([[-1,2],[3, 4]])
a = sess.run(tf.abs(x))
b = sess.run(tf.abs(x_2))
print(a)
print(b)

