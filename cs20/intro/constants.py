import tensorflow as tf



a = tf.constant([2, 2], name='a')

b = tf.constant([[0,1],[2,3]], name='b')

x = tf.multiply(a, b, name='mul')


z = tf.zeros([2, 3], tf.int32 )
o = tf.ones_like(x)

l = tf.lin_space(10.0, 14.0, 8)
print(l)

print(z)
print(o)

with tf.Session() as sess:
    print(sess.run(x))
    print(sess.run(l))


